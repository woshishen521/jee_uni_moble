module.exports = {
	list:
	 [{
	 		"letter": "A",
	 		"data": [{
	 			"name": "鞍山"
	 		}, {
	 			"name": "安顺"
	 		}, {
	 			"name": "阿坝"
	 		}, {
	 			"name": "阿拉善"
	 		}, {
	 			"name": "阿里"
	 		}, {
	 			"name": "安康"
	 		}, {
	 			"name": "阿克苏"
	 		}, {
	 			"name": "安庆"
	 		}, {
	 			"name": "阿勒泰"
	 		}, {
	 			"name": "安阳"
	 		}, {
	 			"name": "澳门"
	 		}]
	 	}
	 
	 	, {
	 		"letter": "B",
	 		"data": [{
	 			"name": "北京"
	 		}, {
	 			"name": "包头"
	 		}, {
	 			"name": "保定"
	 		}, {
	 			"name": "巴中"
	 		}, {
	 			"name": "蚌埠"
	 		}, {
	 			"name": "白银"
	 		}, {
	 			"name": "白城"
	 		}, {
	 			"name": "白山"
	 		}, {
	 			"name": "北海"
	 		}, {
	 			"name": "巴彦淖尔"
	 		}, {
	 			"name": "宝鸡"
	 		}, {
	 			"name": "百色"
	 		}, {
	 			"name": "本溪"
	 		}, {
	 			"name": "保山"
	 		}, {
	 			"name": "毕节"
	 		}, {
	 			"name": "博尔塔拉"
	 		}, {
	 			"name": "滨州"
	 		}, {
	 			"name": "亳州"
	 		}, {
	 			"name": "巴州"
	 		}]
	 	}
	 
	 	, {
	 		"letter": "C",
	 		"data": [{
	 			"name": "成都"
	 		}, {
	 			"name": "重庆"
	 		}, {
	 			"name": "常州"
	 		}, {
	 			"name": "长沙"
	 		}, {
	 			"name": "长春"
	 		}, {
	 			"name": "赤峰"
	 		}, {
	 			"name": "楚雄"
	 		}, {
	 			"name": "长治"
	 		}, {
	 			"name": "巢湖"
	 		}, {
	 			"name": "崇左"
	 		}, {
	 			"name": "潮州"
	 		}, {
	 			"name": "昌吉"
	 		}, {
	 			"name": "沧州"
	 		}, {
	 			"name": "郴州"
	 		}, {
	 			"name": "常德"
	 		}, {
	 			"name": "滁州"
	 		}, {
	 			"name": "朝阳"
	 		}, {
	 			"name": "昌都"
	 		}, {
	 			"name": "池州"
	 		}, {
	 			"name": "承德"
	 		}]
	 	}
	 
	 	, {
	 		"letter": "D",
	 		"data": [{
	 			"name": "东莞"
	 		}, {
	 			"name": "大连"
	 		}, {
	 			"name": "东营"
	 		}, {
	 			"name": "大庆"
	 		}, {
	 			"name": "大同"
	 		}, {
	 			"name": "大理"
	 		}, {
	 			"name": "德阳"
	 		}, {
	 			"name": "迪庆"
	 		}, {
	 			"name": "达州"
	 		}, {
	 			"name": "德州"
	 		}, {
	 			"name": "丹东"
	 		}, {
	 			"name": "大兴安岭"
	 		}, {
	 			"name": "德宏"
	 		}, {
	 			"name": "定西"
	 		}]
	 	}
	 
	 	, {
	 		"letter": "E",
	 		"data": [{
	 			"name": "恩施"
	 		}, {
	 			"name": "鄂州"
	 		}, {
	 			"name": "鄂尔多斯"
	 		}]
	 	}
	 
	 	, {
	 		"letter": "F",
	 		"data": [{
	 			"name": "福州"
	 		}, {
	 			"name": "佛山"
	 		}, {
	 			"name": "抚顺"
	 		}, {
	 			"name": "阜阳"
	 		}, {
	 			"name": "抚州"
	 		}, {
	 			"name": "防城港"
	 		}, {
	 			"name": "阜新"
	 		}]
	 	}
	 
	 	, {
	 		"letter": "G",
	 		"data": [{
	 			"name": "广州"
	 		}, {
	 			"name": "贵阳"
	 		}, {
	 			"name": "桂林"
	 		}, {
	 			"name": "赣州"
	 		}, {
	 			"name": "广元"
	 		}, {
	 			"name": "果洛"
	 		}, {
	 			"name": "固原"
	 		}, {
	 			"name": "甘南"
	 		}, {
	 			"name": "甘孜"
	 		}, {
	 			"name": "广安"
	 		}, {
	 			"name": "贵港"
	 		}]
	 	}
	 
	 	, {
	 		"letter": "H",
	 		"data": [{
	 			"name": "杭州"
	 		}, {
	 			"name": "哈尔滨"
	 		}, {
	 			"name": "合肥"
	 		}, {
	 			"name": "邯郸"
	 		}, {
	 			"name": "惠州"
	 		}, {
	 			"name": "海口"
	 		}, {
	 			"name": "呼和浩特"
	 		}, {
	 			"name": "衡阳"
	 		}, {
	 			"name": "湖州"
	 		}, {
	 			"name": "淮北"
	 		}, {
	 			"name": "鹤岗"
	 		}, {
	 			"name": "海北"
	 		}, {
	 			"name": "海东"
	 		}, {
	 			"name": "黄南"
	 		}, {
	 			"name": "菏泽"
	 		}, {
	 			"name": "海南州"
	 		}, {
	 			"name": "黑河"
	 		}, {
	 			"name": "和田"
	 		}, {
	 			"name": "哈密"
	 		}, {
	 			"name": "淮安"
	 		}, {
	 			"name": "淮南"
	 		}, {
	 			"name": "黄山"
	 		}, {
	 			"name": "海西"
	 		}, {
	 			"name": "贺州"
	 		}, {
	 			"name": "怀化"
	 		}, {
	 			"name": "河池"
	 		}, {
	 			"name": "呼伦贝尔"
	 		}, {
	 			"name": "衡水"
	 		}, {
	 			"name": "河源"
	 		}, {
	 			"name": "红河"
	 		}, {
	 			"name": "汉中"
	 		}, {
	 			"name": "黄冈"
	 		}, {
	 			"name": "黄石"
	 		}, {
	 			"name": "鹤壁"
	 		}, {
	 			"name": "葫芦岛"
	 		}]
	 	}
	 
	 	, {
	 		"letter": "J",
	 		"data": [{
	 			"name": "济南"
	 		}, {
	 			"name": "济宁"
	 		}, {
	 			"name": "嘉兴"
	 		}, {
	 			"name": "金华"
	 		}, {
	 			"name": "焦作"
	 		}, {
	 			"name": "荆州"
	 		}, {
	 			"name": "吉林"
	 		}, {
	 			"name": "锦州"
	 		}, {
	 			"name": "江门"
	 		}, {
	 			"name": "景德镇"
	 		}, {
	 			"name": "吉安"
	 		}, {
	 			"name": "佳木斯"
	 		}, {
	 			"name": "酒泉"
	 		}, {
	 			"name": "金昌"
	 		}, {
	 			"name": "鸡西"
	 		}, {
	 			"name": "济源"
	 		}, {
	 			"name": "晋城"
	 		}, {
	 			"name": "揭阳"
	 		}, {
	 			"name": "晋中"
	 		}, {
	 			"name": "荆门"
	 		}, {
	 			"name": "九江"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "K",
	 		"data": [{
	 			"name": "昆明"
	 		}, {
	 			"name": "昆山"
	 		}, {
	 			"name": "喀什"
	 		}, {
	 			"name": "克拉玛依"
	 		}, {
	 			"name": "开封"
	 		}, {
	 			"name": "克州"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "L",
	 		"data": [{
	 			"name": "兰州"
	 		}, {
	 			"name": "临沂"
	 		}, {
	 			"name": "连云港"
	 		}, {
	 			"name": "聊城"
	 		}, {
	 			"name": "临汾"
	 		}, {
	 			"name": "柳州"
	 		}, {
	 			"name": "洛阳"
	 		}, {
	 			"name": "廊坊"
	 		}, {
	 			"name": "龙岩"
	 		}, {
	 			"name": "六盘水"
	 		}, {
	 			"name": "凉山"
	 		}, {
	 			"name": "六安"
	 		}, {
	 			"name": "丽江"
	 		}, {
	 			"name": "临沧"
	 		}, {
	 			"name": "陇南"
	 		}, {
	 			"name": "拉萨"
	 		}, {
	 			"name": "辽源"
	 		}, {
	 			"name": "辽阳"
	 		}, {
	 			"name": "莱芜"
	 		}, {
	 			"name": "漯河"
	 		}, {
	 			"name": "吕梁"
	 		}, {
	 			"name": "丽水"
	 		}, {
	 			"name": "临夏"
	 		}, {
	 			"name": "林芝"
	 		}, {
	 			"name": "娄底"
	 		}, {
	 			"name": "来宾"
	 		}, {
	 			"name": "泸州"
	 		}, {
	 			"name": "乐山"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "M",
	 		"data": [{
	 			"name": "马鞍山"
	 		}, {
	 			"name": "绵阳"
	 		}, {
	 			"name": "茂名"
	 		}, {
	 			"name": "牡丹江"
	 		}, {
	 			"name": "梅州"
	 		}, {
	 			"name": "眉山"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "N",
	 		"data": [{
	 			"name": "南京"
	 		}, {
	 			"name": "宁波"
	 		}, {
	 			"name": "南通"
	 		}, {
	 			"name": "南宁"
	 		}, {
	 			"name": "南昌"
	 		}, {
	 			"name": "南充"
	 		}, {
	 			"name": "宁德"
	 		}, {
	 			"name": "内江"
	 		}, {
	 			"name": "怒江"
	 		}, {
	 			"name": "南平"
	 		}, {
	 			"name": "南阳"
	 		}, {
	 			"name": "那曲"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "P",
	 		"data": [{
	 			"name": "萍乡"
	 		}, {
	 			"name": "平顶山"
	 		}, {
	 			"name": "莆田"
	 		}, {
	 			"name": "濮阳"
	 		}, {
	 			"name": "攀枝花"
	 		}, {
	 			"name": "平凉"
	 		}, {
	 			"name": "普洱"
	 		}, {
	 			"name": "盘锦"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "Q",
	 		"data": [{
	 			"name": "青岛"
	 		}, {
	 			"name": "泉州"
	 		}, {
	 			"name": "秦皇岛"
	 		}, {
	 			"name": "齐齐哈尔"
	 		}, {
	 			"name": "庆阳"
	 		}, {
	 			"name": "衢州"
	 		}, {
	 			"name": "黔西南"
	 		}, {
	 			"name": "钦州"
	 		}, {
	 			"name": "黔南"
	 		}, {
	 			"name": "曲靖"
	 		}, {
	 			"name": "黔东南"
	 		}, {
	 			"name": "七台河"
	 		}, {
	 			"name": "清远"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "R",
	 		"data": [{
	 			"name": "日照"
	 		}, {
	 			"name": "日喀则"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "S",
	 		"data": [{
	 			"name": "上海"
	 		}, {
	 			"name": "深圳"
	 		}, {
	 			"name": "沈阳"
	 		}, {
	 			"name": "苏州"
	 		}, {
	 			"name": "石家庄"
	 		}, {
	 			"name": "绍兴"
	 		}, {
	 			"name": "顺德"
	 		}, {
	 			"name": "三亚"
	 		}, {
	 			"name": "韶关"
	 		}, {
	 			"name": "绥化"
	 		}, {
	 			"name": "松原"
	 		}, {
	 			"name": "上饶"
	 		}, {
	 			"name": "十堰"
	 		}, {
	 			"name": "三门峡"
	 		}, {
	 			"name": "山南"
	 		}, {
	 			"name": "邵阳"
	 		}, {
	 			"name": "遂宁"
	 		}, {
	 			"name": "商丘"
	 		}, {
	 			"name": "朔州"
	 		}, {
	 			"name": "随州"
	 		}, {
	 			"name": "汕尾"
	 		}, {
	 			"name": "四平"
	 		}, {
	 			"name": "三峡"
	 		}, {
	 			"name": "宿迁"
	 		}, {
	 			"name": "三明"
	 		}, {
	 			"name": "石嘴山"
	 		}, {
	 			"name": "双鸭山"
	 		}, {
	 			"name": "汕头"
	 		}, {
	 			"name": "宿州"
	 		}, {
	 			"name": "商洛"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "T",
	 		"data": [{
	 			"name": "天津"
	 		}, {
	 			"name": "太原"
	 		}, {
	 			"name": "泰安"
	 		}, {
	 			"name": "台州"
	 		}, {
	 			"name": "唐山"
	 		}, {
	 			"name": "泰州"
	 		}, {
	 			"name": "塔城"
	 		}, {
	 			"name": "铜陵"
	 		}, {
	 			"name": "铜川"
	 		}, {
	 			"name": "台北"
	 		}, {
	 			"name": "铜仁"
	 		}, {
	 			"name": "吐鲁番"
	 		}, {
	 			"name": "天水"
	 		}, {
	 			"name": "通辽"
	 		}, {
	 			"name": "铁岭"
	 		}, {
	 			"name": "通化"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "W",
	 		"data": [{
	 			"name": "武汉"
	 		}, {
	 			"name": "无锡"
	 		}, {
	 			"name": "温州"
	 		}, {
	 			"name": "芜湖"
	 		}, {
	 			"name": "威海"
	 		}, {
	 			"name": "潍坊"
	 		}, {
	 			"name": "乌鲁木齐"
	 		}, {
	 			"name": "梧州"
	 		}, {
	 			"name": "吴忠"
	 		}, {
	 			"name": "武威"
	 		}, {
	 			"name": "渭南"
	 		}, {
	 			"name": "乌兰察布"
	 		}, {
	 			"name": "文山"
	 		}, {
	 			"name": "乌海"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "X",
	 		"data": [{
	 			"name": "西安"
	 		}, {
	 			"name": "厦门"
	 		}, {
	 			"name": "徐州"
	 		}, {
	 			"name": "襄阳"
	 		}, {
	 			"name": "西宁"
	 		}, {
	 			"name": "孝感"
	 		}, {
	 			"name": "西双版纳"
	 		}, {
	 			"name": "新余"
	 		}, {
	 			"name": "湘潭"
	 		}, {
	 			"name": "锡林郭勒"
	 		}, {
	 			"name": "兴安"
	 		}, {
	 			"name": "邢台"
	 		}, {
	 			"name": "新乡"
	 		}, {
	 			"name": "湘西"
	 		}, {
	 			"name": "忻州"
	 		}, {
	 			"name": "咸阳"
	 		}, {
	 			"name": "宣城"
	 		}, {
	 			"name": "香港"
	 		}, {
	 			"name": "信阳"
	 		}, {
	 			"name": "咸宁"
	 		}, {
	 			"name": "许昌"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "Y",
	 		"data": [{
	 			"name": "扬州"
	 		}, {
	 			"name": "烟台"
	 		}, {
	 			"name": "盐城"
	 		}, {
	 			"name": "运城"
	 		}, {
	 			"name": "义乌"
	 		}, {
	 			"name": "岳阳"
	 		}, {
	 			"name": "宜昌"
	 		}, {
	 			"name": "玉林"
	 		}, {
	 			"name": "银川"
	 		}, {
	 			"name": "鹰潭"
	 		}, {
	 			"name": "雅安"
	 		}, {
	 			"name": "伊犁"
	 		}, {
	 			"name": "玉树"
	 		}, {
	 			"name": "宜春"
	 		}, {
	 			"name": "营口"
	 		}, {
	 			"name": "永州"
	 		}, {
	 			"name": "宜宾"
	 		}, {
	 			"name": "益阳"
	 		}, {
	 			"name": "玉溪"
	 		}, {
	 			"name": "阳泉"
	 		}, {
	 			"name": "延安"
	 		}, {
	 			"name": "榆林"
	 		}, {
	 			"name": "云浮"
	 		}, {
	 			"name": "延边"
	 		}, {
	 			"name": "阳江"
	 		}, {
	 			"name": "伊春"
	 		}]
	 	}
	 
	 
	 
	 	, {
	 		"letter": "Z",
	 		"data": [{
	 			"name": "郑州"
	 		}, {
	 			"name": "镇江"
	 		}, {
	 			"name": "中山"
	 		}, {
	 			"name": "淄博"
	 		}, {
	 			"name": "珠海"
	 		}, {
	 			"name": "遵义"
	 		}, {
	 			"name": "株洲"
	 		}, {
	 			"name": "自贡"
	 		}, {
	 			"name": "舟山"
	 		}, {
	 			"name": "湛江"
	 		}, {
	 			"name": "肇庆"
	 		}, {
	 			"name": "漳州"
	 		}, {
	 			"name": "张掖"
	 		}, {
	 			"name": "昭通"
	 		}, {
	 			"name": "张家界"
	 		}, {
	 			"name": "周口"
	 		}, {
	 			"name": "驻马店"
	 		}, {
	 			"name": "张家口"
	 		}, {
	 			"name": "资阳"
	 		}, {
	 			"name": "中卫"
	 		}, {
	 			"name": "枣庄"
	 		}]
	 	}
	 ]
}
