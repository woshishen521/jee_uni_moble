/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
 module.exports = {
 	configureWebpack: {
		devServer: {
			port : 8080,
 			disableHostCheck : true,
 			proxy : {
 				"/js" : {
 					target : "http://192.168.43.40:8980",
 					//target : "https://demo.jeesite.com",
 					changeOrigin : true,
 					secure : false
 				}
 			}
 		}
 	},
 	productionSourceMap: false,
 }
